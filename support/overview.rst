Overview
=========

Getting started
---------------

The starting experience for all Beagles has been made to be as
consistent as is possible. For any of the Beagle Linux-based open
hardware computers, visit our `getting started guide <getting-started>`__.

Getting support
---------------

BeagleBoard.org products and `open
hardware <https://www.oshwa.org/definition/>`__ designs are supported
via the on-line community resources. We are very confident in our
community’s ability to provide useful answers in a timely manner. If you
don’t get a productive response within 24 hours, please escalate issues
to Jason Kridner (contact info available on the `About
Page <https://beagleboard.org/about>`__). In case it is needed, Jason
will help escalate issues to suppliers, manufacturers or others. Be sure
to provide a link to your questions on the `community
forums <https://forum.beagleboard.org>`__ as answers will be provided
there.

Diagnostic tools
----------------

Best to be prepared with good diagnostic information to aide with
support.

-  Output of ‘beagle-version’ script needed for support requests
-  `Beagle Tester source <https://git.beagleboard.org/jkridner/beagle-tester>`__

Community resources
-------------------

Please execute the board diagnostics, review the hardware documentation,
and consult the mailing list and IRC channel for support.
BeagleBoard.org is a “community” project with free support only given to
those who are willing to discussing their issues openly for the benefit
of the entire community.

-  `Frequently Asked Questions <https://forum.beagleboard.org/c/faq>`__
-  `Mailing List <https://forum.beagleboard.org>`__
-  `Live Chat <https://beagleboard.org/chat>`__

Consulting and other resources
------------------------------

Need timely response or contract resources because you are building a
product?

-  `Resources <https://beagleboard.org/resources>`__

Repairs
-------

Repairs and replacements only provided on unmodified boards purchased
via an authorized distributor within the first 90 days. All repaired
board will have their flash reset to factory contents. For repairs and
replacements, please contact ‘support’ at BeagleBoard.org using the RMA
form:

-  `RMA request <https://beagleboard.org//support/rma>`__

Understanding Your Beagle
-------------------------

-  `BeagleBone Introduction <https://beagleboard.org/Support/bone101>`__
-  `Hardware <https://beagleboard.org/Support/Hardware+Support>`__
-  `Software <https://beagleboard.org/Support/Software+Support>`__
-  `BoneScript JavaScript
   library <https://beagleboard.org/Support/bonescript>`__
-  `Books <https://beagleboard.org/books>`__

   -  `Exploring BeagleBone <https://beagleboard.org/ebb>`__
   -  `BeagleBone Cookbook <https://beagleboard.org/cookbook>`__
   -  `Bad to the Bone <https://beagleboard.org/bad-to-the-bone>`__

