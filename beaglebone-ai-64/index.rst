.. _bbai64-home:

BeagleBone AI-64
###################

BeagleBone® AI-64 brings a complete system for developing artificial intelligence (AI) and machine learning solutions with the convenience and expandability of the BeagleBone® platform and the peripherals on board to get started right away learning and building applications. With locally hosted, ready-to-use, open-source focused tool chains and development environment, a simple web browser, power source and network connection are all that need to be added to start building performance-optimized embedded applications. Industry-leading expansion possibilities are enabled through familiar BeagleBone® cape headers, with hundreds of open-source hardware examples and dozens of readily available embedded expansion options available off-the-shelf.

.. toctree::
   :maxdepth: 1

   ch01.rst
   ch02.rst
   ch03.rst
   ch04.rst
   ch05.rst
   ch09.rst
   ch10.rst
   ch11.rst
