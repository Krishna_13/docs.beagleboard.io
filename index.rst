..
    BeagleBoard projects documentation main file

.. _bbdocs-home:

BeagleBoard Documentation
############################

Sections
********

.. toctree::
   :maxdepth: 1

   support/index.rst

.. toctree::
   :maxdepth: 1

   contribution/index.rst

.. toctree::
   :maxdepth: 1
   :caption: Boards

   beaglebone-black/index.rst
   beaglebone-ai-64/index.rst
   pocketbeagle/index.rst
   beaglebone-blue/index.rst
   beagleconnect/index.rst

.. toctree::
   :maxdepth: 1
   :caption: Projects

   simppru/index.rst

.. toctree::
   :maxdepth: 1
   :caption: Books

   bone-cook-book/index.rst

Indices and tables
############################

* :ref:`glossary`
* :ref:`genindex`

Indices and Tables
******************

This work is licensed under a `Creative Commons Attribution-ShareAlike 4.0 International License <http://creativecommons.org/licenses/by-sa/4.0/>`_. 

Attribute original work to BeagleBoard.org Foundation.

.. image:: https://licensebuttons.net/l/by-sa/4.0/88x31.png
    :width: 88px
    :height: 31px
    :target: http://creativecommons.org/licenses/by-sa/4.0/
